package my.projects.egs.QuestionnaireProject.services.impl;

import my.projects.egs.QuestionnaireProject.DTOs.CategoryDTO;
import my.projects.egs.QuestionnaireProject.entitys.Category;
import my.projects.egs.QuestionnaireProject.entitys.Test;
import my.projects.egs.QuestionnaireProject.mappers.CategoryMapper;
import my.projects.egs.QuestionnaireProject.repositories.CategoryRepository;
import my.projects.egs.QuestionnaireProject.services.CategoryService;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public CategoryDTO save(Category category) {
        return categoryMapper.categoryToCategoryDto(categoryRepository.save(category));
    }

    @Override
    public CategoryDTO findById(long id) {
        return categoryMapper.categoryToCategoryDto(categoryRepository.findById(id));
    }

    @Override
    public CategoryDTO findByType(String type) {
        return categoryMapper.categoryToCategoryDto(categoryRepository.findByType(type));
    }

    @Override
    public List<CategoryDTO> findAll() {
        return categoryMapper.categoriesToCategoryDtos(categoryRepository.findAll());
    }
}
