package my.projects.egs.QuestionnaireProject.services.impl;

import my.projects.egs.QuestionnaireProject.DTOs.AnswerDTO;
import my.projects.egs.QuestionnaireProject.DTOs.ChosenAnswersDTO;
import my.projects.egs.QuestionnaireProject.entitys.Answer;
import my.projects.egs.QuestionnaireProject.mappers.AnswerMapper;
import my.projects.egs.QuestionnaireProject.repositories.AnswerRepository;
import my.projects.egs.QuestionnaireProject.services.AnswerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService {

    private final AnswerRepository answerRepository;
    private final AnswerMapper answerMapper;

    public AnswerServiceImpl(AnswerRepository answerRepository, AnswerMapper answerMapper) {
        this.answerRepository = answerRepository;
        this.answerMapper = answerMapper;
    }

    @Override
    public AnswerDTO save(Answer answer) {
        return answerMapper.answerToAnswerDto(answerRepository.save(answer));
    }

    @Override
    public AnswerDTO findById(long id) {
        return answerMapper.answerToAnswerDto(answerRepository.findById(id));
    }

    @Override
    public List<AnswerDTO> findAll() {
        return answerMapper.answersToAnswerDtos(answerRepository.findAll());
    }

    @Override
    public List<AnswerDTO> findByQuestionId(long id) {
        return answerMapper.answersToAnswerDtos(answerRepository.findByQuestionId(id));
    }

}
