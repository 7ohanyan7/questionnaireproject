package my.projects.egs.QuestionnaireProject.entitys;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "answer")
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    Long id;

    @Column(name = "answer")
    String answer;

    @Column(name = "isTrue")
    Boolean isTrue;

    @ManyToOne
    @JoinColumn(name = "question_id")
    Question question;
}
