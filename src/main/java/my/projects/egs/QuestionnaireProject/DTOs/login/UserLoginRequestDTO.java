package my.projects.egs.QuestionnaireProject.DTOs.login;


import lombok.*;

import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class UserLoginRequestDTO {

    @Size(min = 1, message = "The email field can't be empty")
    private String email;

    @Size(min = 1, message = "The password field can't be empty")
    private String password;


}
