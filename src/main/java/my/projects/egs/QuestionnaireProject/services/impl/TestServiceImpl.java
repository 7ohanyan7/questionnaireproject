package my.projects.egs.QuestionnaireProject.services.impl;

import my.projects.egs.QuestionnaireProject.DTOs.TestDTO;
import my.projects.egs.QuestionnaireProject.entitys.Test;
import my.projects.egs.QuestionnaireProject.mappers.TestMapper;
import my.projects.egs.QuestionnaireProject.repositories.TestRepository;
import my.projects.egs.QuestionnaireProject.services.TestService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {

    private final TestRepository testRepository;
    private final TestMapper testMapper;

    public TestServiceImpl(TestRepository testRepository, TestMapper testMapper) {
        this.testRepository = testRepository;
        this.testMapper = testMapper;
    }


    @Override
    public TestDTO save(Test test) {
        return testMapper.testToTestDto(testRepository.save(test));
    }

    @Override
    public TestDTO findById(long id) {
        return testMapper.testToTestDto(testRepository.findById(id));
    }

    @Override
    public List<TestDTO> findAll() {
        return testMapper.testsToTestDtos(testRepository.findAll());
    }

    @Override
    public List<TestDTO> findByCategoryId(long id) {
        return testMapper.testsToTestDtos(testRepository.findByCategoryId(id));
    }

    @Override
    public List<TestDTO> findByCategoryType(String type) {
        return testMapper.testsToTestDtos(testRepository.findByCategoryType(type));
    }

    @Override
    public TestDTO findByTest(String test) {
        return testMapper.testToTestDto(testRepository.findByTitle(test));
    }


}
