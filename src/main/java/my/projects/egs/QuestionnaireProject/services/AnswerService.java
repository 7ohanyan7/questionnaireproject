package my.projects.egs.QuestionnaireProject.services;

import my.projects.egs.QuestionnaireProject.DTOs.AnswerDTO;
import my.projects.egs.QuestionnaireProject.DTOs.ChosenAnswersDTO;
import my.projects.egs.QuestionnaireProject.entitys.Answer;

import java.util.List;

public interface AnswerService {

    AnswerDTO save(Answer answer);

    AnswerDTO findById(long id);

    List<AnswerDTO> findAll();

    List<AnswerDTO> findByQuestionId(long id);

}
