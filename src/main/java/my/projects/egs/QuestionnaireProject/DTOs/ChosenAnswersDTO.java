package my.projects.egs.QuestionnaireProject.DTOs;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ChosenAnswersDTO {
    private List<AnswerDTO> chosenAnswers;
    private static Long testId;

    public void addChosenAnswer(AnswerDTO answerDTO){
        this.chosenAnswers.add(answerDTO);
    }

    public static void setTestId(Long testId) {
        ChosenAnswersDTO.testId = testId;
    }

    public static Long getTestId() {
        return testId;
    }
}
