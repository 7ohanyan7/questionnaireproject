package my.projects.egs.QuestionnaireProject.services;

import my.projects.egs.QuestionnaireProject.DTOs.register.UserRegisterRequestDTO;
import my.projects.egs.QuestionnaireProject.DTOs.register.UserRegisterResponseDTO;
import my.projects.egs.QuestionnaireProject.entitys.user.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService {

    User save(User user);

    User findByID(long id);

    User findByEmail(String email);

    List<User> findAll();

    UserRegisterResponseDTO register(UserRegisterRequestDTO userRegisterRequestDTO);


}
