package my.projects.egs.QuestionnaireProject.DTOs;

import lombok.Data;
import lombok.NoArgsConstructor;
import my.projects.egs.QuestionnaireProject.DTOs.login.UserLoginResponseDTO;

@Data
@NoArgsConstructor
public class ResultDTO {

    Long id;

    Long userId;

    Integer result;

    String test;

    Long testId;

    String userName;
}
