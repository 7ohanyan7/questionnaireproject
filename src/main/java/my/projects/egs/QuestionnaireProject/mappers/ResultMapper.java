package my.projects.egs.QuestionnaireProject.mappers;

import my.projects.egs.QuestionnaireProject.DTOs.ResultDTO;
import my.projects.egs.QuestionnaireProject.entitys.Result;
import my.projects.egs.QuestionnaireProject.entitys.Test;
import my.projects.egs.QuestionnaireProject.entitys.user.User;
import my.projects.egs.QuestionnaireProject.services.TestService;
import my.projects.egs.QuestionnaireProject.services.UserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public abstract class ResultMapper {

    @Autowired
    UserService userService;
    @Autowired
    TestMapper testMapper;
    @Autowired
    TestService testService;


    public Result resultDtoToResult(ResultDTO resultDTO){
        if ( resultDTO == null ) {
            return null;
        }

        Result result = new Result();

        result.setId( resultDTO.getId() );
        result.setResult( resultDTO.getResult() );

        Test test = testMapper.testDtoToTest( testService.findById( resultDTO.getTestId() ) );
        test.setId(resultDTO.getTestId());
        User user = userService.findByID( resultDTO.getUserId() );
        user.setId(resultDTO.getUserId());

        result.setTest( test );
        result.setUser( user );

        return result;
    }

    public abstract List<Result> resultDtosToResults(List<ResultDTO> resultDTOS);

    @Mapping(target = "testId", expression = "java( result.getTest().getId() )")
    @Mapping(target = "test", expression = "java( result.getTest().getTitle() )")
    @Mapping(target = "userId", expression = "java( result.getUser().getId() )")
    @Mapping(target = "userName", expression = "java( result.getUser().getName() )")
    public abstract ResultDTO resultToResultDto(Result result);

    public abstract List<ResultDTO> resultsToResultDTOS(List<Result> results);
}
