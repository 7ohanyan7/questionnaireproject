package my.projects.egs.QuestionnaireProject.DTOs.login;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.projects.egs.QuestionnaireProject.DTOs.ResultDTO;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
public class UserLoginResponseDTO {

    private String name;

    private String errorMassage;

    private List<ResultDTO> resultDTOS;

}
