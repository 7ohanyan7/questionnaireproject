package my.projects.egs.QuestionnaireProject.entitys;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    Long id;

    @Column(name = "question")
    String question;

    @OneToMany(mappedBy = "question")
    List<Answer> answers;

    @ManyToOne
    @JoinColumn(name = "test_id")
    Test test;

}
