package my.projects.egs.QuestionnaireProject.DTOs;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class TestDTO {

    Long id;

    String title;

    List<QuestionDTO> questionDTOS;

    CategoryDTO categoryDTO;


}
