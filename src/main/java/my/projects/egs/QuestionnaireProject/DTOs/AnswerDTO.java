package my.projects.egs.QuestionnaireProject.DTOs;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AnswerDTO {

    Long id;

    String answer;

    Boolean isTrue;

}
