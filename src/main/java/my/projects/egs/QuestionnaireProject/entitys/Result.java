package my.projects.egs.QuestionnaireProject.entitys;

import lombok.Data;
import lombok.NoArgsConstructor;
import my.projects.egs.QuestionnaireProject.entitys.user.User;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "result")
public class Result {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    Long id;

    @Column(name = "result")
    Integer result;

    @ManyToOne
    @JoinColumn(name = "test_id")
    Test test;

    @ManyToOne
    @JoinColumn(name = "user_id")
    User user;

}
