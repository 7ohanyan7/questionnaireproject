package my.projects.egs.QuestionnaireProject.DTOs;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class QuestionDTO {

    Long id;

    String question;

    List<AnswerDTO> answerDTOS;

}
