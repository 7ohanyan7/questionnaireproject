package my.projects.egs.QuestionnaireProject.repositories;

import my.projects.egs.QuestionnaireProject.entitys.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    Question findById(long id);

    List<Question> findByTest_Id(long id);

}
