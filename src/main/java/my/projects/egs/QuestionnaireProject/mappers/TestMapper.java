package my.projects.egs.QuestionnaireProject.mappers;

import my.projects.egs.QuestionnaireProject.DTOs.TestDTO;
import my.projects.egs.QuestionnaireProject.entitys.Question;
import my.projects.egs.QuestionnaireProject.entitys.Test;
import my.projects.egs.QuestionnaireProject.services.CategoryService;
import my.projects.egs.QuestionnaireProject.services.QuestionService;
import my.projects.egs.QuestionnaireProject.services.impl.CategoryServiceImpl;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public abstract class TestMapper {

    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    QuestionMapper questionMapper;
    @Autowired
    QuestionService questionService;

    @Mapping(target = "questions", expression = "java( questionMapper.questionDtosToQuestions( testDTO.getQuestionDTOS() ) )")
    @Mapping(target = "category", expression = "java( categoryMapper.categoryDtoToCategory(testDTO.getCategoryDTO() ) )")
    public abstract Test testDtoToTest(TestDTO testDTO);

    public abstract List<Test> testDtosToTests(List<TestDTO> testDTOS);

    @Mapping(target = "questionDTOS", expression = "java( questionService.findByTest_Id( test.getId() ) )")
    @Mapping(target = "categoryDTO", expression = "java( categoryMapper.categoryToCategoryDto( test.getCategory() ) )")
    public abstract TestDTO testToTestDto(Test test);

    public abstract List<TestDTO> testsToTestDtos(List<Test> tests);

}
