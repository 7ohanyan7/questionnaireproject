package my.projects.egs.QuestionnaireProject.repositories;

import my.projects.egs.QuestionnaireProject.entitys.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findById(long id);

    User findByEmail(String email);

}
