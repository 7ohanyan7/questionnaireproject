package my.projects.egs.QuestionnaireProject.controllers;

import my.projects.egs.QuestionnaireProject.DTOs.CategoryDTO;
import my.projects.egs.QuestionnaireProject.entitys.Test;
import my.projects.egs.QuestionnaireProject.services.CategoryService;
import my.projects.egs.QuestionnaireProject.services.TestService;
import my.projects.egs.QuestionnaireProject.services.impl.TestServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class CategoryController {

    private final CategoryService categoryService;
    private final TestService testService;

    public CategoryController(CategoryService categoryService, TestService testService) {
        this.categoryService = categoryService;
        this.testService = testService;
    }


    @RequestMapping(method = RequestMethod.GET, value = "/categories")
    public String categories(Model model){

        model.addAttribute("categories",categoryService.findAll());


        return "categories";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/categories/{category}")
    public String category(Model model, @PathVariable String category){

        CategoryDTO categoryDTO = categoryService.findByType(category);

        model.addAttribute("category", categoryDTO.getType());
        model.addAttribute("tests", testService.findByCategoryId(categoryDTO.getId()));
        return "category";
    }
}
