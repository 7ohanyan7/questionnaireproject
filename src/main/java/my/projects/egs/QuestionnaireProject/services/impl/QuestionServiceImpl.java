package my.projects.egs.QuestionnaireProject.services.impl;

import my.projects.egs.QuestionnaireProject.DTOs.QuestionDTO;
import my.projects.egs.QuestionnaireProject.entitys.Question;
import my.projects.egs.QuestionnaireProject.mappers.QuestionMapper;
import my.projects.egs.QuestionnaireProject.repositories.QuestionRepository;
import my.projects.egs.QuestionnaireProject.services.QuestionService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final QuestionMapper questionMapper;

    public QuestionServiceImpl(QuestionRepository questionRepository, QuestionMapper questionMapper) {
        this.questionRepository = questionRepository;
        this.questionMapper = questionMapper;
    }


    @Override
    public QuestionDTO save(Question question) {
        return questionMapper.questionToQuestionDto(questionRepository.save(question));
    }

    @Override
    public QuestionDTO findById(long id) {
        return questionMapper.questionToQuestionDto(questionRepository.findById(id));
    }

    @Override
    public List<QuestionDTO> findAll() {
        return questionMapper.questionsToQuestionDtos(questionRepository.findAll());
    }

    @Override
    public List<QuestionDTO> findByTest_Id(long id) {
        return questionMapper.questionsToQuestionDtos(questionRepository.findByTest_Id(id));
    }
}
