package my.projects.egs.QuestionnaireProject.services.impl;

import my.projects.egs.QuestionnaireProject.DTOs.login.UserLoginRequestDTO;
import my.projects.egs.QuestionnaireProject.DTOs.login.UserLoginResponseDTO;
import my.projects.egs.QuestionnaireProject.entitys.user.User;
import my.projects.egs.QuestionnaireProject.mappers.UserMapper;
import my.projects.egs.QuestionnaireProject.services.LoginService;
import my.projects.egs.QuestionnaireProject.services.UserService;
import my.projects.egs.QuestionnaireProject.validators.LoginValidator;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {


    private final AuthenticationManager authenticationManager;
    private final LoginValidator loginValidator;
    private final UserService userService;
    private final UserMapper userMapper;

    public LoginServiceImpl(AuthenticationManager authenticationManager, LoginValidator loginValidator, UserService userService, UserMapper userMapper) {
        this.authenticationManager = authenticationManager;
        this.loginValidator = loginValidator;
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @Override
    public UserLoginResponseDTO login(UserLoginRequestDTO userLoginRequestDTO) {


        String errorMessage = loginValidator.validate(userLoginRequestDTO);

        UserLoginResponseDTO userLoginResponseDTO = new UserLoginResponseDTO();
        if (errorMessage == null)
        {
            UserDetails userEntity = userService.loadUserByUsername(userLoginRequestDTO.getEmail());
            Authentication authentication = new UsernamePasswordAuthenticationToken(userEntity, userEntity.getPassword(), userEntity.getAuthorities());
            authenticationManager.authenticate(authentication);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            userLoginResponseDTO = userMapper.userToUserLoginResponseDto((User) userEntity);
        }
        userLoginResponseDTO.setErrorMassage(errorMessage);

        return userLoginResponseDTO;
    }

}
