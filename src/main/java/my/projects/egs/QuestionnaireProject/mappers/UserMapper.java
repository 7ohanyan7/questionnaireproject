package my.projects.egs.QuestionnaireProject.mappers;

import my.projects.egs.QuestionnaireProject.DTOs.login.UserLoginRequestDTO;
import my.projects.egs.QuestionnaireProject.DTOs.login.UserLoginResponseDTO;
import my.projects.egs.QuestionnaireProject.DTOs.register.UserRegisterRequestDTO;
import my.projects.egs.QuestionnaireProject.DTOs.register.UserRegisterResponseDTO;
import my.projects.egs.QuestionnaireProject.entitys.user.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public abstract class UserMapper {

    public abstract User userLoginRequestDtoToUser(UserLoginRequestDTO userLoginRequestDTO);

    public abstract User userLoginResponseDtoToUser(UserLoginResponseDTO userLoginResponseDTO);

    public abstract User userRegisterRequestDtoToUser(UserRegisterRequestDTO userRegisterRequestDTO);

    public abstract User userRegisterResponseDtoToUser(UserRegisterResponseDTO userRegisterResponseDTO);

    public abstract List<User> userLoginRequestDtosToUsers(List<UserLoginRequestDTO> userLoginRequestDTOS);

    public abstract List<User> userLoginResponseDtosToUsers(List<UserLoginResponseDTO> userLoginResponseDTOS);

    public abstract List<User> userRegisterRequestDtosToUsers(List<UserRegisterRequestDTO> userRegisterRequestDTOS);

    public abstract List<User> userRegisterResponseDtosToUsers(List<UserRegisterResponseDTO> userRegisterResponseDTOS);

    public abstract UserLoginRequestDTO userToUserLoginRequestDto(User user);

    public abstract UserLoginResponseDTO userToUserLoginResponseDto(User user);

    public abstract UserRegisterRequestDTO userToUserRegisterRequestDto(User user);

    public abstract UserRegisterResponseDTO userToUserRegisterResponseDto(User user);

    public abstract List<UserLoginRequestDTO> usersToUserLoginRequestDtos(List<User> users);

    public abstract List<UserLoginResponseDTO> usersToUserLoginResponseDtos(List<User> users);

    public abstract List<UserRegisterRequestDTO> usersToUserRegisterRequestDtos(List<User> users);

    public abstract List<UserRegisterResponseDTO> usersToUserRegisterResponseDtos(List<User> users);

}
