package my.projects.egs.QuestionnaireProject.mappers;


import my.projects.egs.QuestionnaireProject.DTOs.QuestionDTO;
import my.projects.egs.QuestionnaireProject.entitys.Question;
import my.projects.egs.QuestionnaireProject.services.AnswerService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring")
public abstract class QuestionMapper {

    @Autowired
    AnswerMapper answerMapper;
    @Autowired
    AnswerService answerService;


    @Mapping(target = "answers", expression = "java( answerMapper.answerDtosToAnswers( questionDTO.getAnswerDTOS() ) )")
    public abstract Question questionDtoToQuestion(QuestionDTO questionDTO);

    public abstract List<Question> questionDtosToQuestions(List<QuestionDTO> questionDTOS);

    @Mapping(target = "answerDTOS", expression = "java( answerService.findByQuestionId( question.getId() ) )")
    public abstract QuestionDTO questionToQuestionDto(Question question);

    public abstract List<QuestionDTO> questionsToQuestionDtos(List<Question> questions);

}


