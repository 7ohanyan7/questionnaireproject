package my.projects.egs.QuestionnaireProject.mappers;

import my.projects.egs.QuestionnaireProject.DTOs.UserResponseForAdminDTO;
import my.projects.egs.QuestionnaireProject.entitys.user.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public abstract class UserResponseForAdminMapper {

    @Autowired
    ResultMapper resultMapper;

    @Mapping(target = "resultDTOS", expression = "java( resultMapper.resultsToResultDTOS( user.getResults() ) )")
    public abstract UserResponseForAdminDTO userToUserResponseForAdminDto(User user);

    public abstract List<UserResponseForAdminDTO> usersToUserResponseForAdminDtos(List<User> users);
}
