package my.projects.egs.QuestionnaireProject.services.impl;

import my.projects.egs.QuestionnaireProject.DTOs.register.UserRegisterRequestDTO;
import my.projects.egs.QuestionnaireProject.DTOs.register.UserRegisterResponseDTO;
import my.projects.egs.QuestionnaireProject.entitys.user.Role;
import my.projects.egs.QuestionnaireProject.entitys.user.User;
import my.projects.egs.QuestionnaireProject.mappers.UserMapper;
import my.projects.egs.QuestionnaireProject.repositories.UserRepository;
import my.projects.egs.QuestionnaireProject.services.UserService;
import my.projects.egs.QuestionnaireProject.validators.RegisterValidator;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RegisterValidator registerValidator;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    public UserServiceImpl(UserRepository userRepository, RegisterValidator registerValidator, PasswordEncoder passwordEncoder, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.registerValidator = registerValidator;
        this.passwordEncoder = passwordEncoder;
        this.userMapper = userMapper;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findByID(long id) {
        return userRepository.findById(id);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public UserRegisterResponseDTO register(UserRegisterRequestDTO userRegisterRequestDTO) {

        String errorMessage = registerValidator.validate(userRegisterRequestDTO);

        if (errorMessage == null)
        {
            userRegisterRequestDTO.setRoles(Collections.singleton(Role.USER));
            userRegisterRequestDTO.setPassword(passwordEncoder.encode(userRegisterRequestDTO.getPassword()));
            save(userMapper.userRegisterRequestDtoToUser(userRegisterRequestDTO));
        }

        UserRegisterResponseDTO userRegisterResponseDTO = userMapper.userToUserRegisterResponseDto(userMapper.userRegisterRequestDtoToUser(userRegisterRequestDTO));
        userRegisterResponseDTO.setErrorMassage(errorMessage);

        return userRegisterResponseDTO;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return findByEmail(s);
    }
}
