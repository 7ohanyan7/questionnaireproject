package my.projects.egs.QuestionnaireProject.DTOs.register;


import lombok.*;
import my.projects.egs.QuestionnaireProject.entitys.user.Role;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@NoArgsConstructor
public class UserRegisterRequestDTO {

    @Size(min = 3, max = 20, message = "The name must be < 20 and > 3")
    private String name;

    @Email(message = "Not match for email")
    @Size(min = 1, message = "Email can't be empty")
    private String email;

    @Size(min = 8, message = "The password must be greater than 8")
    private String password;

    private Set<Role> roles;


}
