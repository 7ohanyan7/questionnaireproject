package my.projects.egs.QuestionnaireProject.validators;

import my.projects.egs.QuestionnaireProject.DTOs.register.UserRegisterRequestDTO;
import my.projects.egs.QuestionnaireProject.repositories.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class RegisterValidator {

    private final UserRepository userRepository;

    public RegisterValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public String validate(UserRegisterRequestDTO userRegisterRequestDTO) {
        String errorMessage = validateEmail(userRegisterRequestDTO.getEmail());
        return errorMessage;
    }

    private String validateEmail(String email){
        if (userRepository.findByEmail(email) == null) {
            return null;
        }
        else return "Email already exists";
    }


}
