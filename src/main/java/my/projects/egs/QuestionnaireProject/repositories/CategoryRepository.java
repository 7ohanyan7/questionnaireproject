package my.projects.egs.QuestionnaireProject.repositories;

import my.projects.egs.QuestionnaireProject.entitys.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category findById(long id);

    Category findByType(String type);

}
