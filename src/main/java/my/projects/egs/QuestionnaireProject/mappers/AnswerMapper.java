package my.projects.egs.QuestionnaireProject.mappers;

import my.projects.egs.QuestionnaireProject.DTOs.AnswerDTO;
import my.projects.egs.QuestionnaireProject.entitys.Answer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper
public interface AnswerMapper {

    Answer answerDtoToAnswer(AnswerDTO answerDTO);

    List<Answer> answerDtosToAnswers(List<AnswerDTO> answerDTOS);

    AnswerDTO answerToAnswerDto(Answer answer);

    List<AnswerDTO> answersToAnswerDtos(List<Answer> answers);

}

