package my.projects.egs.QuestionnaireProject.services;

import my.projects.egs.QuestionnaireProject.DTOs.CategoryDTO;
import my.projects.egs.QuestionnaireProject.entitys.Category;

import java.util.List;

public interface CategoryService {

    CategoryDTO save(Category category);

    CategoryDTO findById(long id);

    CategoryDTO findByType(String type);

    List<CategoryDTO> findAll();

}
