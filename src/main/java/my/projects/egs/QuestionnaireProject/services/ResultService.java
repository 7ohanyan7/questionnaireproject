package my.projects.egs.QuestionnaireProject.services;

import my.projects.egs.QuestionnaireProject.DTOs.ChosenAnswersDTO;
import my.projects.egs.QuestionnaireProject.DTOs.ResultDTO;
import my.projects.egs.QuestionnaireProject.entitys.Result;

import java.util.List;

public interface ResultService {

    ResultDTO save(Result result);

    ResultDTO findById(long id);

    List<ResultDTO> findAll();

    Integer countPoints(ChosenAnswersDTO chosenAnswersDTO);

    List<ResultDTO> findByUserId(long id);

    List<ResultDTO> getResults();


}
