package my.projects.egs.QuestionnaireProject.repositories;

import my.projects.egs.QuestionnaireProject.entitys.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
    Answer findById(long id);

    List<Answer> findByQuestionId(long id);
}

