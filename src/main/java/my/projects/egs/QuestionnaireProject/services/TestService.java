package my.projects.egs.QuestionnaireProject.services;

import my.projects.egs.QuestionnaireProject.DTOs.TestDTO;
import my.projects.egs.QuestionnaireProject.entitys.Test;

import java.util.List;

public interface TestService {

    TestDTO save(Test test);

    TestDTO findById(long id);

    List<TestDTO> findAll();

    List<TestDTO> findByCategoryId(long id);

    List<TestDTO> findByCategoryType(String type);

    TestDTO findByTest(String test);
}
