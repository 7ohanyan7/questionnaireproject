package my.projects.egs.QuestionnaireProject.services;

import my.projects.egs.QuestionnaireProject.DTOs.QuestionDTO;
import my.projects.egs.QuestionnaireProject.entitys.Question;

import java.util.List;

public interface QuestionService {
    
    QuestionDTO save(Question question);

    QuestionDTO findById(long id);

    List<QuestionDTO> findAll();

    List<QuestionDTO> findByTest_Id(long id);
}
