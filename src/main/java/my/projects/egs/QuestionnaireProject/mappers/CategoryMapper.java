package my.projects.egs.QuestionnaireProject.mappers;

import my.projects.egs.QuestionnaireProject.DTOs.CategoryDTO;
import my.projects.egs.QuestionnaireProject.DTOs.TestDTO;
import my.projects.egs.QuestionnaireProject.entitys.Category;
import my.projects.egs.QuestionnaireProject.entitys.Test;
import my.projects.egs.QuestionnaireProject.services.TestService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Mapper(componentModel = "spring")
public abstract class CategoryMapper {



    public abstract Category categoryDtoToCategory(CategoryDTO categoryDTO);

    public abstract List<Category> categoryDtosToCategories(List<CategoryDTO> categoryDTOS);

    public abstract CategoryDTO categoryToCategoryDto(Category category);

    public abstract List<CategoryDTO> categoriesToCategoryDtos(List<Category> categories);

}
