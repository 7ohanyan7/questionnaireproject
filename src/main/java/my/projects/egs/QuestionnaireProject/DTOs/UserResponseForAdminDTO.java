package my.projects.egs.QuestionnaireProject.DTOs;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class UserResponseForAdminDTO {

    Long id;

    String name;

    String email;

    List<ResultDTO> resultDTOS;

}
