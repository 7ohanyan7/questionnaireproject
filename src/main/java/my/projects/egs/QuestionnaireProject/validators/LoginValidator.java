package my.projects.egs.QuestionnaireProject.validators;

import my.projects.egs.QuestionnaireProject.DTOs.login.UserLoginRequestDTO;
import my.projects.egs.QuestionnaireProject.mappers.UserMapper;
import my.projects.egs.QuestionnaireProject.repositories.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class LoginValidator {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    public LoginValidator(UserRepository userRepository, PasswordEncoder passwordEncoder, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userMapper = userMapper;
    }

    public String validate(UserLoginRequestDTO userLoginRequestDTO) {
        String errorMassage = null;
        if ( (errorMassage = validateEmail(userLoginRequestDTO.getEmail())) != null)
        {
            return errorMassage;
        }
        errorMassage = validatePassword(userLoginRequestDTO.getEmail(), userLoginRequestDTO.getPassword());
        return errorMassage;
    }

    private String validateEmail(String email) {
        if (userRepository.findByEmail(email) != null)
        {
            return null;
        } else return "Invalid email";
    }

    private String validatePassword(String email, String password){
        System.out.println("Pass validation");
        UserLoginRequestDTO userLoginRequestDTO = userMapper.userToUserLoginRequestDto(userRepository.findByEmail(email));
        String passwordEncoded = passwordEncoder.encode(password);
        if (userLoginRequestDTO.getPassword().equals(passwordEncoded))
        {
            System.out.println("Success pass validation");
            return null;
        } return "Invalid password";
    }
}
