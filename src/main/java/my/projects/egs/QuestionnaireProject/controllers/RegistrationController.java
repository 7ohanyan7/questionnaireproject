package my.projects.egs.QuestionnaireProject.controllers;

import my.projects.egs.QuestionnaireProject.DTOs.register.UserRegisterRequestDTO;
import my.projects.egs.QuestionnaireProject.DTOs.register.UserRegisterResponseDTO;
import my.projects.egs.QuestionnaireProject.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private final UserService userService;

    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET,value = {"/registration", "/"})
    public String register(Model model){

        UserRegisterRequestDTO userRegisterRequestDTO = new UserRegisterRequestDTO();
        model.addAttribute("user",userRegisterRequestDTO);

        System.out.println("register GET");
        return "register";
    }

    @RequestMapping(method = RequestMethod.POST,value = "/registration")
    public String register(@Valid @ModelAttribute("user") UserRegisterRequestDTO userRegisterRequestDTO, BindingResult bindingResult, Model model){

        System.out.println("register POST");

        if (bindingResult.hasErrors())
        {
            System.out.println(bindingResult.getAllErrors());
            return "register";
        }

        UserRegisterResponseDTO userRegisterResponseDTO = userService.register(userRegisterRequestDTO);
        String errorMessage = userRegisterResponseDTO.getErrorMassage();
        if (errorMessage != null)
        {
            model.addAttribute("errorMessage", errorMessage);
        } else {
            model.addAttribute("message", "Successful registration");
        }

        return "register";

    }


}
