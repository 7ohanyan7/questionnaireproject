package my.projects.egs.QuestionnaireProject.controllers;

import my.projects.egs.QuestionnaireProject.DTOs.ChosenAnswersDTO;
import my.projects.egs.QuestionnaireProject.DTOs.ResultDTO;
import my.projects.egs.QuestionnaireProject.entitys.user.User;
import my.projects.egs.QuestionnaireProject.services.AnswerService;
import my.projects.egs.QuestionnaireProject.services.ResultService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class ResultController {

    private final ResultService resultService;
    private final AnswerService answerService;

    public ResultController(ResultService resultService, AnswerService answerService) {
        this.resultService = resultService;
        this.answerService = answerService;
    }


    @RequestMapping(method = RequestMethod.POST, value = "/result")
    public String result(@ModelAttribute ChosenAnswersDTO chosenAnswersDTO, Model model){

        System.out.println(chosenAnswersDTO);
        System.out.println(ChosenAnswersDTO.getTestId());


        Integer result = resultService.countPoints(chosenAnswersDTO);
        model.addAttribute("result", result);


        return "result";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/results")
    public String results(Model model){

        List<ResultDTO> resultDTOS = resultService.getResults();
        model.addAttribute("results", resultDTOS);


        return "results";
    }
}
