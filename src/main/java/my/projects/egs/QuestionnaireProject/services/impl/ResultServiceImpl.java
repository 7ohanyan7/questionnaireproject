package my.projects.egs.QuestionnaireProject.services.impl;

import my.projects.egs.QuestionnaireProject.DTOs.AnswerDTO;
import my.projects.egs.QuestionnaireProject.DTOs.ChosenAnswersDTO;
import my.projects.egs.QuestionnaireProject.DTOs.ResultDTO;
import my.projects.egs.QuestionnaireProject.DTOs.TestDTO;
import my.projects.egs.QuestionnaireProject.DTOs.login.UserLoginResponseDTO;
import my.projects.egs.QuestionnaireProject.entitys.Result;
import my.projects.egs.QuestionnaireProject.entitys.user.User;
import my.projects.egs.QuestionnaireProject.mappers.ResultMapper;
import my.projects.egs.QuestionnaireProject.repositories.ResultRepository;
import my.projects.egs.QuestionnaireProject.services.AnswerService;
import my.projects.egs.QuestionnaireProject.services.ResultService;
import my.projects.egs.QuestionnaireProject.services.TestService;
import my.projects.egs.QuestionnaireProject.services.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResultServiceImpl implements ResultService {

    private final ResultRepository resultRepository;
    private final ResultMapper resultMapper;
    private final AnswerService answerService;
    private final TestService testService;
    private final UserService userService;

    public ResultServiceImpl(ResultRepository resultRepository, ResultMapper resultMapper, AnswerService answerService, TestService testService, UserService userService) {
        this.resultRepository = resultRepository;
        this.resultMapper = resultMapper;
        this.answerService = answerService;
        this.testService = testService;
        this.userService = userService;
    }

    @Override
    public ResultDTO save(Result result) {
        return resultMapper.resultToResultDto(resultRepository.save(result));
    }

    @Override
    public ResultDTO findById(long id) {
        return resultMapper.resultToResultDto(resultRepository.findById(id));
    }

    @Override
    public List<ResultDTO> findAll() {
        return resultMapper.resultsToResultDTOS(resultRepository.findAll());
    }

    @Override
    public Integer countPoints(ChosenAnswersDTO chosenAnswersDTO){

        Integer result = 0;

        for (AnswerDTO answerDTO: chosenAnswersDTO.getChosenAnswers())
        {
            if (answerService.findById(answerDTO.getId()).getIsTrue())
            {
                ++result;
            }
        }

        ResultDTO resultDTO = new ResultDTO();
        resultDTO.setResult(result);
        Long testId = testService.findById(ChosenAnswersDTO.getTestId()).getId();
        resultDTO.setTestId(testId);

        Long userId = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof User)
        {
            userId = userService.findByEmail(((User) principal).getEmail()).getId();
            resultDTO.setUserId(userId);
        }

        System.out.println(save(resultMapper.resultDtoToResult(resultDTO)));
        return result;

    }

    @Override
    public List<ResultDTO> findByUserId(long id) {
        return resultMapper.resultsToResultDTOS(resultRepository.findByUserId(id));
    }

    @Override
    public List<ResultDTO> getResults() {

        List<ResultDTO> resultDTOS = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof User)
        {
            resultDTOS = findByUserId(userService.findByEmail(((User) principal).getEmail()).getId());
        }

        return resultDTOS;
    }

}
