package my.projects.egs.QuestionnaireProject.controllers;

import my.projects.egs.QuestionnaireProject.DTOs.UserResponseForAdminDTO;
import my.projects.egs.QuestionnaireProject.mappers.UserMapper;
import my.projects.egs.QuestionnaireProject.mappers.UserResponseForAdminMapper;
import my.projects.egs.QuestionnaireProject.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class UserController {

    private final UserService userService;
    private final UserResponseForAdminMapper userResponseForAdminMapper;

    public UserController(UserService userService, UserResponseForAdminMapper userResponseForAdminMapper) {
        this.userService = userService;
        this.userResponseForAdminMapper = userResponseForAdminMapper;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/admin")
    public String admin(){

        return "admin";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users")
    public String users(Model model){

        model.addAttribute("users", userResponseForAdminMapper.usersToUserResponseForAdminDtos(userService.findAll()));


        return "users";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/usersresults")
    public String usersResults(Model model){

        List<UserResponseForAdminDTO> users = userResponseForAdminMapper.usersToUserResponseForAdminDtos(userService.findAll());

        model.addAttribute("users",users);


        return "usersresults";
    }
}
