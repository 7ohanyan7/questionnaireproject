package my.projects.egs.QuestionnaireProject.DTOs.register;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class UserRegisterResponseDTO {

    private String name;

    private String email;

    private String errorMassage;

}
