package my.projects.egs.QuestionnaireProject.controllers;

import my.projects.egs.QuestionnaireProject.DTOs.login.UserLoginRequestDTO;
import my.projects.egs.QuestionnaireProject.DTOs.login.UserLoginResponseDTO;
import my.projects.egs.QuestionnaireProject.services.LoginService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class LoginController {

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "login")
    public String login(Model model){

        System.out.println("login - GET");

        UserLoginRequestDTO userLoginRequestDTO = new UserLoginRequestDTO();
        model.addAttribute("user", userLoginRequestDTO);

        return "login";
    }

    @RequestMapping(method = RequestMethod.POST, value = "login")
    public String login(@Valid @ModelAttribute("user") UserLoginRequestDTO userLoginRequestDTO, BindingResult bindingResult, Model model){

        System.out.println("login - POST");

        if (bindingResult.hasErrors())
        {
            System.out.println(bindingResult.getAllErrors());
            return "login";
        }

        UserLoginResponseDTO userLoginResponseDTO = loginService.login(userLoginRequestDTO);
        if (userLoginResponseDTO.getErrorMassage() != null)
        {
            model.addAttribute("errorMessage", userLoginResponseDTO.getErrorMassage());
            return "login";
        }

        model.addAttribute("user", userLoginResponseDTO);
        return "home";


    }

    @RequestMapping(method = RequestMethod.GET, value = "home")
    public String home(Model model){


        System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());

        return "home";

    }
}
