package my.projects.egs.QuestionnaireProject.repositories;

import my.projects.egs.QuestionnaireProject.entitys.Result;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultRepository extends JpaRepository<Result, Long> {

    Result findById(long id);

    List<Result> findByUserId(long id);
}
