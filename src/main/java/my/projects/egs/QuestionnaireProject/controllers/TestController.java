package my.projects.egs.QuestionnaireProject.controllers;

import my.projects.egs.QuestionnaireProject.DTOs.AnswerDTO;
import my.projects.egs.QuestionnaireProject.DTOs.ChosenAnswersDTO;
import my.projects.egs.QuestionnaireProject.DTOs.QuestionDTO;
import my.projects.egs.QuestionnaireProject.services.TestService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TestController {

    private final TestService testService;

    public TestController(TestService testService) {
        this.testService = testService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "categories/{category}/{test}/{id}")
    public String test( Model model, @PathVariable String category, @PathVariable String test, @PathVariable Long id) {

        System.out.println(testService.findById(id).getQuestionDTOS());
        model.addAttribute("questions", testService.findById(id).getQuestionDTOS());
        ChosenAnswersDTO chosenForm = new ChosenAnswersDTO();
        ChosenAnswersDTO.setTestId(id);

        model.addAttribute("chosenForm", chosenForm);

        return "test";
    }



}

