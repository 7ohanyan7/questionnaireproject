package my.projects.egs.QuestionnaireProject.services;

import my.projects.egs.QuestionnaireProject.DTOs.login.UserLoginRequestDTO;
import my.projects.egs.QuestionnaireProject.DTOs.login.UserLoginResponseDTO;

public interface LoginService {

    UserLoginResponseDTO login(UserLoginRequestDTO userLoginRequestDTO);

}
